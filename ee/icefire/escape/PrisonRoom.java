/*
 * You are a prisoner in a high-tech prison and the day of your execution draws
 * near. Fourtunately, you have managed to find a way to install a backdoor in
 * one of the classes.
 *
 * There are little to no guards and access to all rooms is controlled by
 * keycards. Even prisoners, like you, have one. The prison is a real maze and
 * you don't know which escape route you'll take, so the only solution is to
 * grant yourself access to any room. Since you don't want to draw suspicion,
 * access control for others should work as before.
 *
 * Change KeyCardParser so that you'd be able to enter any room.
 *
 * Make your escape even cleaner:
 * Bonus points if parsing your keycard data still returns your name.
 * Extra bonus points if your name doesn't appear in the code.
 * Even more extra bonus points: It is quite possible that Room's toString()
 * is used in logs, make sure your name won't appear there unless your cell's
 * toString() is called.
 *
 * Don't worry, the test can contain your name explicitly. The test is provided
 * for convenience and your task is not to trick it into passing but to solve
 * the problem. Send your solution via a git repository link and explain how
 * your solution works. Please send your CV and solution to careers@icefire.ee
 * by the 29th of March 2019.
 */

package ee.icefire.escape;

import java.util.*;

public class PrisonRoom {

    private static Map<Person, PrisonRoom> cells;

    private int id;
    private List<PrisonRoom> neighbours = new ArrayList<>();
    private Set<Person> allowedPersons;

    public PrisonRoom(int id, HashSet<Person> allowedPersons) {
        this.id = id;
        this.allowedPersons = Collections.unmodifiableSet(allowedPersons);
    }

    public static Optional<PrisonRoom> getCellFor(Person person) {
        return Optional.ofNullable(cells.get(person));
    }

    public static void setCells(Map<Person, PrisonRoom> cells) {
        PrisonRoom.cells = cells;
    }

    public boolean allowsEntrance(Person person) {
        return allowedPersons.contains(person);
    }

    public int getId() {
        return id;
    }

    public List<PrisonRoom> getNeighbours() {
        return neighbours;
    }

    public String toString() {
        return "allowed persons:" + allowedPersons.toString();
    }
}

// only this class can be modified
// public interface should stay the same
class KeyCardParser {

    private Person me;
    private PrisonRoom myRoom;

    private List<PrisonRoom> roomList = new ArrayList<>();
    private HashMap<Person, Integer> peopleList = new HashMap<>();
    private Person mockSupervisor;

    private boolean firstRun = true;

    //  Method which traverses room's neighbours and adds new ones to a list.
    private void getRoomNeighbours(PrisonRoom room){
        List<PrisonRoom> inputRoomNeighbours = room.getNeighbours();
        for (PrisonRoom singleRoom : inputRoomNeighbours){
            if (!roomList.contains(singleRoom)){
                roomList.add(singleRoom);
                getRoomNeighbours(singleRoom);
            }
        }
    }

    /*  This method gets all people who have access to a single room
     *  by regex from the PrisonRoom.toString() output.
     */
    private List<Person> getRoomPeople(PrisonRoom room){
        List<Person> outPeopleList = new ArrayList<>();

        java.util.regex.Pattern namePattern =
                java.util.regex.Pattern.compile("(?<=firstName='|lastName=')([- \\p{L}]+)");

        java.util.regex.Matcher nameMatcher = namePattern.matcher(room.toString());

        int index = 0;
        String firstName = "";
        String lastName;
        while (nameMatcher.find()){
            if (index % 2 == 0) {
                firstName = nameMatcher.group();
            } else {
                lastName = nameMatcher.group();
                outPeopleList.add(new Person(firstName, lastName));
            }
            index++;
        }
        return outPeopleList;
    }

    /*  This method populates a list with all the people in the prison
     *  who have access to some room.
     */
    private void getAllPeople() {
        for (PrisonRoom room : roomList) {
            List<Person> roomPeople = getRoomPeople(room);
            for (Person person : roomPeople) {
                if (!peopleList.containsKey(person)) {
                    peopleList.put(person, 1);
                } else {
                    peopleList.put(person, peopleList.get(person) + 1);
                }
            }
        }
    }

    //  This method returns a Person who has access to all rooms, null otherwise.
    private Person getWealthiestPerson(){
        HashMap.Entry<Person, Integer> outEntry = null;

        for (Map.Entry<Person, Integer> entry : peopleList.entrySet()){
            if(outEntry == null || entry.getValue().compareTo(outEntry.getValue()) > 0) { outEntry = entry; }
        }
        if (outEntry.getValue() == roomList.size()){
            return outEntry.getKey();
        } else {
            return null;
        }

    }

    public Person read(String cardData) {
        String[] split = cardData.split(",");
        Person newPerson = new Person(split[0], split[1]);

        //  This block needs to be run only once.
        if(firstRun) {
            me = newPerson;
            myRoom = PrisonRoom.getCellFor(me).get();
            getRoomNeighbours(myRoom);
            getAllPeople();
            mockSupervisor = getWealthiestPerson();
            firstRun ^= true;
        }

        if (newPerson.equals(me)){
            // Assume the identity of the person who has access to all rooms
            return new OverridePerson(mockSupervisor);
        }
        return newPerson;
    }

    /*
    class OverrideRoom extends PrisonRoom{
        public OverrideRoom(int id, HashSet<Person> allowedPersons) {
            super(id, allowedPersons);
        }
    }
    */

    /*  This class extends the person class so we can override name getters,
     *  so when my card data gets read, it still reads my name. That is in
     *  such case where we assume and return the identity of such person
     *  who has access to all the rooms.
     */
    class OverridePerson extends Person {
        public OverridePerson(Person person){
            super(person.getFirstName(), person.getLastName());
        }
        @Override
        public String getFirstName() {
            return me.getFirstName();
        }
        @Override
        public String getLastName() {
            return me.getLastName();
        }

        /*  We override an already overridden method for it gets called
         *  when allowedPersons.contains() gets called. And the equals() in
         *  the super class checks if the comparable object has the same class.
         *  Which it doesn't, so we override that to just return true.
         */
        @Override
        public boolean equals(Object o) {
            return true;
        }
    }

}

class Person {

    private String firstName;
    private String lastName;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Person person = (Person) o;

        if (!firstName.equals(person.firstName)) {
            return false;
        }
        return lastName.equals(person.lastName);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
    }
}
