# prax_icefire

Final solution to prison problem.

We start by getting myCell object, from which we can traverse the neighbours tree and add all new ones to a list. 
Next we get every person who has access to some room by calling `toString()` on every room we previously got. We do some regex to get the first and lastnames from those strings (any unicode character and spacebar and dash one or more times) and generate new Person objects, which also get added to the list. From that list we find the person who has access to all rooms(supposedly warden then).
We create a class which extends Person class and override some methods so we can return warden as an object of that class in `read()`. This means that if `getFirstName()` or `getLastName()` is called on that object it always returns my name. 
We also override `equals()`, for that gets called when `allowedPersons.contains()` is called. And it checks if comparable objects have same class which doesn't work for us.

This solution is far from perfect, because right now we still cling to the hope that there exists a person who has access to all the rooms. I couldn't think of anything else. I tried setting cells to include me in every cell but Map doesn't like duplicate keys nor take in List of rooms. It seemed to me that `shouldAllowMeInEveryRoom()` test checks for `allowsEntrance()` using the rooms object it initially generated. So even if I managed to add myself to allowedPersons set the test wouldn't know anything about it for it's checking the rooms object from `setUp()`. If I'm wrong about this I'd love to know more about how it actually works.
So all in all if this problem has a perfect solution, or better even, I'd like to see that. Just so I could learn from that.